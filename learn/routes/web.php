<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'Admin\HomeController@index')->name('home');

Route::get('/admin/user/profile/{id}', 'Admin\UserController@show')->name('profile');
Route::post('/admin/user/edit/{id}', 'Admin\UserController@edit')->name('editUser');

//BLOG
Route::get('/admin/blog', 'Admin\BlogController@index')->name('blog');
Route::post('/admin/blog', 'Admin\BlogController@index');
Route::get('/admin/blog/add', 'Admin\BlogController@create')->name('addBlog');
Route::post('/admin/blog/add', 'Admin\BlogController@store');
Route::get('/admin/blog/edit/{id}', 'Admin\BlogController@show')->name('showBlog');
Route::post('/admin/blog/edit/{id}', 'Admin\BlogController@update')->name('editBlog');
Route::get('/admin/blog/delete/{id}', 'Admin\BlogController@destroy')->name('delBlog');


//FRONTENDDDDDD

//Blog

Route::get('/blog', 'Frontend\BlogController@index')->name('fblog');
Route::post('/blog', 'Frontend\BlogController@index');
Route::get('/blog-detail/{id}', 'Frontend\BlogController@show')->name('detailBlog');

Route::get('/member/login', 'Frontend\LoginController@showLogin')->name('flogin');
Route::post('/member/login', 'Frontend\LoginController@doLogin');
Route::get('/','Frontend\MemberController@index')->name('index');
Route::get('/{id}','Frontend\MemberController@detail')->name('detail');

Route::get('/member/cart','Frontend\MemberController@cart')->name('cart');

Route::post('/member/order','Frontend\MemberController@order')->name('order');

Route::get('/member/checkout','Frontend\MemberController@checkout')->name('checkout');

Route::post('/member/pay','Frontend\MemberController@payOneStep')->name('payOneStep');
Route::post('/member/signup', 'Frontend\MemberController@create')->name('signup');

Route::post('/member/search','Frontend\ProductController@search')->name('fsearch');

Route::group(
    [
        'prefix' => 'member',
        'middleware'=>  'member'
    ],function(){
        Route::get('/product', 'Frontend\ProductController@index')->name('listProduct');
        Route::get('/product/edit/{id}', 'Frontend\ProductController@show')->name('editProduct');
        Route::post('/product/edit/{id}', 'Frontend\ProductController@update')->name('editProduct');
        Route::get('/product/create', 'Frontend\ProductController@create')->name('addProduct');
        Route::post('/product/create', 'Frontend\ProductController@store')->name('addProduct');
        Route::get('/product/delete/{id}', 'Frontend\ProductController@destroy')->name('delProduct');

        Route::post('/comment','Frontend\BlogController@comment');
        Route::post('/rate','Frontend\BlogController@rate');
        Route::get('/logout', 'Frontend\LoginController@logout')->name('flogout');
        
        Route::get('/edit/{id}', 'Frontend\MemberController@show')->name('fprofile');
        Route::post('/edit/{id}', 'Frontend\MemberController@update')->name('fprofile');
    }
);


