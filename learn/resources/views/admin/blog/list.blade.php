
        @extends('admin.template.master')
        @section('content')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Blog</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">List Product</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                   
                    <div class="col-12">
                        <div class="card">
                            <div class="table-responsive">
                            @if(count($blog_data) > 0)
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Created At</th>
                                            <th scope="col">Description</th>
											<th scope="col">Content</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                            @foreach($blog_data as $row)
                                                <tr>
                                                    <td scope="col">{{$row->id}}</td>
                                                    <td scope="col">{{mb_strimwidth($row->title, 0, 10, '…')}}</td>
                                                    <td scope="col">{{$row->created_at}}</td>
                                                    <td scope="col">{{mb_strimwidth($row->description, 0, 40, '…')}}</td>
                                                    <td scope="col">{{mb_strimwidth($row->content, 0, 40, '…')}}</td>
                                                    <td scope="col">
                                                        <a href="{{route('showBlog',[$row->id])}}" ><b class="m-r-1 mdi mdi-eye"></b><span> View</span></a>
                                                        <a href="{{route('delBlog',[$row->id])}}" ><b class="m-r-1 mdi mdi-delete"></b><span> Delete</span></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        
                                    </tbody>
                                       
                                </table>
                                {{$blog_data->render()}}
                                    <input type="hidden" value="{{$blog_data->currentPage()}}" name="currentPage">
                                @endif
                            </div>
                            
                        </div>
                        <div class="col-12">
                                    
                                    <div class='col-12' style="text-align:right; color:white">
                                        <a href="{{route('addBlog')}}" class="btn btn-success" type="submit">Add Blog</a>
                                    </div>
                                </div>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- <script>
                $(document).ready(function(){
                    
                    $(document).on("keyup","input[name='search']",function(){
                        let key = $(this).val();
                        let currentPage = $("input[name='currentPage']").val();
                        let data = JSON.stringify({
                                'key' : key,
                                'currentPage' : currentPage
                            });

                        $.ajax({
                            url: '/admin/blog',
                            data: {
                                 'data' : data,
                                 _token: '{{csrf_token()}}'
                            },
                            success: function(data) {
                                let blogs = JSON.parse(data);
                                let content = '';
                                blogs.data.map(function(value){
                                    content += '<tr>'+value.id
                                                    +'<td scope="col">'+value.id+'</td>'
                                                    +'<td scope="col">{{mb_strimwidth('+value.title+', 0, 10, '…')}}</td>'
                                                    +'<td scope="col">'+value.created_at+'</td>'
                                                    +'<td scope="col">{{mb_strimwidth('+value.description+', 0, 40, '…')}}</td>'
                                                    +'<td scope="col">{{mb_strimwidth('+value.content+', 0, 40, '…')}}</td>'
                                                    +'<td scope="col">'
                                                        +'<a href="{{route("showBlog",['+value.id+'])}}" ><b class="m-r-1 mdi mdi-eye"></b><span> View</span></a>'
                                                        +'<a href="{{route("delBlog",['+value.id+'])}}" ><b class="m-r-1 mdi mdi-delete"></b><span> Delete</span></a>'
                                                    +'</td>'
                                                +'</tr>';
                                    return content;
                                })
                                console.log(content);
                                $(".table").find("tbody").html(content);
                            },
                            type: 'POST'
                         });
                    })
                })
            </script> -->
            @endsection
            