@include('admin.template.header')

@include('admin.template.left-bar')

@yield('content')

@include('admin.template.footer')