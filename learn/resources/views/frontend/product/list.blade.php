
	@include('frontend.template.header')
	@include('frontend.template.left-control')
				<div class="col-sm-9">
					<h3>List Product</h3>
					<section id="cart_items">
						<div class="container col-sm-12" >

							<div class="table-responsive cart_info">
								@if(count($listProduct))
								<table class="table table-condensed">
									<thead>
										<tr class="cart_menu">
											<td class="description">Id</td>
											<td class="description">Name</td>
											<td class="description">Category</td>
											<td class="description">Brand</td>
											<td class="image">Image</td>
											<td class="quantity">Price</td>
											<td colspan="2" class="total">Action</td>
											
										</tr>
									</thead>
									<tbody>
										@foreach($listProduct as $pro)
										<tr>
											<td>
												<h4>{{$pro->id}}</h4>
											</td>
											<td>
												<h4>{{$pro->name}}</h4>
											</td>
											<td>
												<h4>{{$pro->categorys->name}}</h4>
											</td>
											<td>
												<h4>{{$pro->brands->name}}</h4>
											</td>
											<td class="cart_product">
												@foreach(json_decode($pro->image) as $img)
													<img src="{{asset('upload/product/'.Auth::user()->id.'/'.$img)}}" width="40px" height="40px" alt="">
												@endforeach
											</td>
											<td class="cart_price">
												<p>{{$pro->price}}</p>
											</td>
											<td>
												<a href="{{route('editProduct',[$pro->id])}}"><i class="glyphicon glyphicon-edit"></i></a>
											</td>
											<td class="cart_delete">
												<a class="cart_quantity_delete" href="{{route('delProduct',[$pro->id])}}"><i class="fa fa-times"></i></a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								@else
									<h1>No Product</h1>

								@endif
							</div>
							
						</div>
					</section>
					<div class="col-sm-12">
						<div class="signup-form"><!--sign up form-->
								<button onclick="location.href='{{route('addProduct')}}'" style="float: right;" name="submit" class="btn btn-default">Create</button>
						
						</div><!--/sign up form-->
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	@include('frontend.template.footer')