
    @include('frontend.template.header')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Shopping Cart</li>
				</ol>
			</div>

            
                <div class="table-responsive cart_info">
                @if(session()->exists('cart') && count(session('cart')['listOrder']))
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="description">Item</td>
                                <td class="description">Name</td>
                                <td class="description">Price</td>
                                <td class="price">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                        
                                    
                                    @foreach(session('cart')['listOrder'] as $key => $value)
                                        <tr>
                                            <td class='cart_product'>"
                                                <a href=''><img width='70px' heigth='70px' src='{{asset("upload/product/".$value->user->id."/".json_decode($value->image)[0])}}' alt=''></a>
                                            </td>
                                            <td class='cart_description'>
                                                <h4><a href=''>{{$value->name}}</a></h4>
                                                <p>Web ID: {{$value->id}}</p>
                                            </td>
                                            <td class='cart_price'>
                                                <p>${{$value->price}}</p>
                                            </td>
                                            <td class='cart_quantity'>
                                                <div class='cart_quantity_button'>
                                                    <a id='{{$key}}' class='cart_quantity_up' href='javascript:void(0)'> + </a>
                                                    <input id='{{$key}}'  class='cart_quantity_input' type='text' name='quantity' value='{{$value->count}}' autocomplete='off' size='2'>
                                                    <a id='{{$key}}' class='cart_quantity_down' href='javascript:void(0)'> - </a>
                                                </div>
                                            </td>
                                            <td class='cart_total'>
                                                <p class='cart_total_price'>${{$value->price * $value->count}}</p>
                                            </td>
                                            <td class='cart_delete'>
                                                <a id='{{$key}}' class='cart_quantity_delete' href='javascript:void(0)'><i class='fa fa-times'></i></a>
                                            </td>
                                        </tr>
                                        
                                    @endforeach
                                
                            
                            
                        </tbody>
                    </table>
                    @else
                        <h1>No Product in your Cart<h1>

                    @endif
                </div>
            


		</div>
	</section>
	<!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your
					delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>

							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>

							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>${{(session()->exists('cart')) ? session('cart')['total'] : 0}}</span></li>
						</ul>
						<a class="btn btn-default update" href="">Update</a>
						<a class="btn btn-default check_out" href="{{route('checkout')}}">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#do_action-->

	<script>
		$(document).ready(function (){
				$(document).on("click", ".cart_quantity_up", function () {
                    let count = $(this).next().val(parseInt($(this).next().val()) + 1);
					changeQuality($(this), "+");
				})
				$(document).on("click", ".cart_quantity_down", function () {
					changeQuality($(this) , "-");
                    if( $(this).prev().val() == 1){
                        removeProduct($(this));
                    }
                    let count = $(this).prev().val(parseInt($(this).prev().val()) - 1);
				})
				$(document).on("click", ".cart_quantity_delete", function () {
					changeQuality($(this), "del");
                    removeProduct($(this));
				})
				$(document).on("change", ".cart_quantity_input", function () {
					changeQuality($(this), null);
                    if(ele.target.value <= 0){
                        removeProduct($(this));
                    }
				})

                function removeProduct(ele){
                    $(ele).parents("tr").remove();
                        $(".countCart").html(parseInt($(".countCart").html()) - 1);
                }
				
			function changeQuality(ele , math){
				let id = $(ele).attr("id");
				math = (math == null) ? ele.parent().children("input").val() : math;
				$.ajax({
						url: '{{route("order")}}',
						type: 'POST',
						data: {
							'data' : JSON.stringify({
                                action : 'change',
							    id: id,
							    math: math
                            }),
                            _token : '{{csrf_token()}}'
						},
						error: function() {
							alert("Change Cart Failed !");
						},
						
						success: function(data) {
							let res = JSON.parse(data);
							if(res.product){
								$(ele).parents(".cart_quantity").next().children(".cart_total_price").html("$"+
									res.product.price * res.product.count);	
							}
							
                            $(document).find(".total_area ul li:nth-child(4) span").html("$"+res.totalBill);
						}
						
				});
			}

			
		})


			

	</script>
	@include('frontend.template.footer')