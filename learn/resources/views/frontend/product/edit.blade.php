
	@include('frontend.template.header')
	@include('frontend.template.left-control')
				<div class="col-sm-9">
					<h3>Edit Product</h3>
					<div class="signup-form"><!--sign up form-->
					@if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
						<form action="" method="post" enctype="multipart/form-data">
							@csrf
							<input type="text" value="{{$product->name}}" name="name" placeholder="Name"/>
							<input type="text" value="{{$product->price}}" name="price" placeholder="Price"/>
							<div>
      
                                            <select name="idCategory" >
												@foreach($listCategories as $cate)
													<option selected="{{($product->idCategpry == $cate->id) ? selected : ''}}" value="{{$cate->id}}">{{$cate->name}}</option>
												@endforeach
                                            </select>
                                       
									</div>
									<div>
                                        
                                            <select name="idBrand" >
											@foreach($listBrands as $brand)
													<option {{($product->idBrands == $brand->id) ? selected : ''}} value="{{$brand->id}}">{{$brand->name}}</option>
												@endforeach
                                            </select>
                                       
                                    </div>
									<div>
                                            <select name="status" >
												<option @if($product->status == 0) selected @endif  value="0">New</option>
												<option @if($product->status == 1) selected @endif value="1">Sale</option>	
                                            </select>
                                        </div>
								
                                        
									<div style="display:none">
										<input style="width:200px;display:inline-block" name="sale" type="text" value="{{$product->sale}}"/>  $
									</div>
									<input type="text" value="{{$product->company}}" name="company" placeholder="Company"/>
							<input type="file" name="image[]" placeholder="Image Product" multiple/>
							@if(count(json_decode($product->image)))
							<div>
								@foreach(json_decode($product->image) as $img)
								<div style="width: 60px;display:inline-block">
									<img width="60px" src="{{asset('upload/product/'.Auth::user()->id.'/'.$img)}}" alt="">
									<input checked name="imgEdit[]" type="checkbox" value="{{$img}}">	
								</div>
								@endforeach
							</div>
							@endif

							<textarea name="description">{{$product->description}}</textarea>
							
							<input type="hidden" name="tmp">
							<button type="submit" name="submit" class="btn btn-default">Create</button>
						</form>
					</div><!--/sign up form-->
					
				</div>	
			</div>
		</div>
	</section>
	
	@include('frontend.template.footer')