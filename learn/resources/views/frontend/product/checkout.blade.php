@include('frontend.template.header')

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input {{($errors->any()) ? 'checked' : ''}} id="register" type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
						<div class="col-sm-12">
							<div {{($errors->any()) ? '' : 'style=display:none'}} id="signup"   class="signup-form"><!--sign up form-->
								<h2>New User Signup!</h2>
								@if ($errors->any())
										<div class="alert alert-danger">
											<ul>
												@foreach ($errors->all() as $error)
													<li>{{ $error }}</li>
												@endforeach
											</ul>
										</div>
									@endif
								<form action="/member/signup" method="POST" enctype="multipart/form-data">
									@csrf
									<input type="text" name="name" value="{{old('name')}}" placeholder="Name"/>
									<input type="email" value="{{old('email')}}" name="email" placeholder="Email Address"/>
									<input type="password" name="password" placeholder="Password"/>
									<!-- @error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror -->
									<input type="password" name="repassword" placeholder="RePassword"/>
									<input type="text" value="{{old('phone')}}" name="phone" placeholder="Phone"/>
									<input type="text" value="{{old('address')}}" name="address" placeholder="Address"/>
									<input type="file" name="avatar" />
									<div>
												<label >Select Country</label>
												<div>
													<select name="idCountry" >
													
													</select>
												</div>
											</div>
											<button id="doSignup" style="visibility:hidden" type="submit" name="submit" class="btn btn-default">Signup</button>    
								</form>
									
							</div><!--/sign up form-->	
						</div>
						<div class="col-sm-3">
							<div class="shopper-info">
								<a id="finish" class="btn btn-primary" href="javascript:void(0)">Continue</a>
							</div>
						</div>
					</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>


			<div class="table-responsive cart_info">
            @if(session()->exists('cart') && count(session('cart')['listOrder']))
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						
                    @foreach(session('cart')['listOrder'] as $key => $value)
                                        <tr>
                                            <td class='cart_product'>"
                                                <a href=''><img width='70px' heigth='70px' src='{{asset("upload/product/".$value->user->id."/".json_decode($value->image)[0])}}' alt=''></a>
                                            </td>
                                            <td class='cart_description'>
                                                <h4><a href=''>{{$value->name}}</a></h4>
                                                <p>Web ID: {{$value->id}}</p>
                                            </td>
                                            <td class='cart_price'>
                                                <p>${{$value->price}}</p>
                                            </td>
                                            <td class='cart_quantity'>
                                                <div class='cart_quantity_button'>
                                                    <a id='{{$key}}' class='cart_quantity_up' href='javascript:void(0)'> + </a>
                                                    <input id='{{$key}}'  class='cart_quantity_input' type='text' name='quantity' value='{{$value->count}}' autocomplete='off' size='2'>
                                                    <a id='{{$key}}' class='cart_quantity_down' href='javascript:void(0)'> - </a>
                                                </div>
                                            </td>
                                            <td class='cart_total'>
                                                <p class='cart_total_price'>${{$value->price * $value->count}}</p>
                                            </td>
                                            <td class='cart_delete'>
                                                <a id='{{$key}}' class='cart_quantity_delete' href='javascript:void(0)'><i class='fa fa-times'></i></a>
                                            </td>
                                        </tr>
                                        
                            @endforeach


						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>${{(session()->exists('cart')) ? session('cart')['total'] : 0}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
                @else
                        <h1>No Product in your Cart<h1>

                @endif
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->

	<form style="visibility:hidden" action="{{route('payOneStep')}}" method="POST">
									@csrf
									<button id="doSubmit" style="visibility:hidden" type="submit" name="submit" class="btn btn-default">Signup</button>    
								</form>

	<script>
		$(document).ready(function(){
			$(document).on("click","#finish",function(){
				if($(document).find("#register").is(":checked")){
					
					$("#doSignup").click();
				}else{
					if("{{!empty(Auth::user())}}"){
						$("#doSubmit").click();
					}else{
						location.href="{{route('flogin')}}";
					}
				}
			})			
		})
	</script>

    <script>
        $(document).ready(function(){
            $(document).on("change","#register",function(){
                if($(document).find("#register").is(":checked")){
                    $(document).find("#signup").show("slow");
                }else{
                    $(document).find("#signup").hide("slow");
                }
            })
        })

    </script>
	@include('frontend.template.footer')

	