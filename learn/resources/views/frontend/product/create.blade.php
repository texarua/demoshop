
	@include('frontend.template.header')
	@include('frontend.template.left-control')
				<div class="col-sm-9">
					<h3>Create Product</h3>
					<div class="signup-form"><!--sign up form-->
					@if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
						<form action="#" method="post" enctype="multipart/form-data">
							@csrf
							<input type="text" value="{{old('name')}}" name="name" placeholder="Name"/>
							<input type="text" value="{{old('price')}}" name="price" placeholder="Price"/>
							<div>
      
                                            <select name="idCategory" >
												@foreach($listCategories as $cate)
													<option value="{{$cate->id}}">{{$cate->name}}</option>
												@endforeach
                                            </select>
                                       
									</div>
									<div>
                                        
                                            <select name="idBrand" >
											@foreach($listBrands as $brand)
													<option value="{{$brand->id}}">{{$brand->name}}</option>
												@endforeach
                                            </select>
                                       
                                    </div>

								
                                        <div>
                                            <select name="status" >
												<option value="0">New</option>
												<option value="1">Sale</option>	
                                            </select>
                                        </div>
									<div style="display:none">
										<input style="width:200px;display:inline-block" name="sale" type="text" value="0"/>  $
									</div>
									<input type="text" value="{{old('company')}}" name="company" placeholder="Company"/>
							<input type="file" name="image[]" placeholder="Image Product" multiple/>
							<textarea name="description">{{old('name')}}</textarea>
							<button type="submit" name="submit" class="btn btn-default">Create</button>
						</form>
					</div><!--/sign up form-->
					
				</div>	
			</div>
		</div>
	</section>
	
	@include('frontend.template.footer')