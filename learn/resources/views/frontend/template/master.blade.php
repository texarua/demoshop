@include('frontend.template.header')

@include('frontend.template.left-bar')

@yield('content')

@include('frontend.template.footer')