<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Blog | E-Shopper</title>
    <link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/animate.css')}}" rel="stylesheet">
	<link href="{{asset('public/css/main.css')}}" rel="stylesheet">
	<link href="{{asset('public/css/responsive.css')}}" rel="stylesheet">
	<link href="{{asset('public/rate/css/rate.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
	
	<script src="{{asset('public/js/jquery.js')}}"></script>
    <link rel="shortcut icon" href="{{asset('public/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('public/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('public/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('public/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('public/images/ico/apple-touch-icon-57-precomposed.png')}}">
    
    <style>
        table{
            border : 2px;
            background-color: chocolate;
        }

</style>
</head><!--/head-->

	<section id="cart_items">
		<div class="container"> 
                <div class="table-responsive cart_info">
                @if(isset($listDetail['listOrder']) && count($listDetail['listOrder']))
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="description">Item</td>
                                <td class="description">Name</td>
                                <td class="description">Price</td>
                                <td class="price">Quantity</td>
                                <td class="total">Total</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>  
                                    @foreach($listDetail['listOrder'] as $key => $value)
                                        <tr>
                                            <td class='cart_product'>"
                                                <a href=''><img width='70px' heigth='70px' src='{{asset("upload/product/".$value->user->id."/".json_decode($value->image)[0])}}' alt=''></a>
                                            </td>
                                            <td class='cart_description'>
                                                <h4><a href=''>{{$value->name}}</a></h4>
                                                <p>Web ID: {{$value->id}}</p>
                                            </td>
                                            <td class='cart_price'>
                                                <p>${{$value->price}}</p>
                                            </td>
                                            <td class='cart_quantity'>
                                                <div class='cart_quantity_button'>
                                                    <a id='{{$key}}' class='cart_quantity_up' href='javascript:void(0)'> + </a>
                                                    <input id='{{$key}}'  class='cart_quantity_input' type='text' name='quantity' value='{{$value->count}}' autocomplete='off' size='2'>
                                                    <a id='{{$key}}' class='cart_quantity_down' href='javascript:void(0)'> - </a>
                                                </div>
                                            </td>
                                            <td class='cart_total'>
                                                <p class='cart_total_price'>${{$value->price * $value->count}}</p>
                                            </td>
                                            <td class='cart_delete'>
                                                <a id='{{$key}}' class='cart_quantity_delete' href='javascript:void(0)'><i class='fa fa-times'></i></a>
                                            </td>
                                        </tr>
                                        
                                    @endforeach
                        </tbody>
                    </table>
                    @else
                        <h1>No Product in your Cart<h1>

                    @endif
                </div>
		</div>
	</section>
	<!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Cart Sub Total <span>$59</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li>Total <span>{{$listDetail['total']}}</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#do_action-->

    <br/><br/>
<footer id="footer"><!--Footer-->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	

  
    
	<script src="{{asset('public/js/price-range.js')}}"></script>
    <script src="{{asset('public/js/jquery.scrollUp.min.js')}}"></script>
	<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('public/js/main.js')}}"></script>
</body>
</html>