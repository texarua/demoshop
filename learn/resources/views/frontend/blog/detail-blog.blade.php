
	@extends('frontend.template.master')
    @section('content')
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$data->title}}</h3>
							<div class="post-meta">
								<ul>
                                    <li><i class="fa fa-user"></i> {{$data->user->name}}</li>
								    <li><i class="fa fa-clock-o"></i> {{date('h:i A', strtotime($data->created_at))}}</li>
									<li><i class="fa fa-calendar"></i>{{date('M d, Y', strtotime($data->created_at))}} </li>
								</ul>
								<?php
										$avgRate = $data->Rates->avg('rate');
									?>
									<span id="rateInfor">
										<?php
											
												for($i=0; $i<5; $i++){
													echo '<i class="fa fa-star'.
														($avgRate==$i+.5?'-half':'').
														($avgRate<=$i?'-o':'').
														'" aria-hidden="true"></i>';
													echo "\n";
												}
										?>
									</span>
							</div>
							<a href="">
                                <img src="{{asset('upload/'.$data->image)}}" alt="">
							</a>
							<?php
                                echo html_entity_decode($data->description);
                            ?>

                            <?php
                                echo html_entity_decode($data->content);
                            ?>
							<div class="pager-area">
								<ul class="pager pull-right">
									<li><a href="#">Pre</a></li>
									<li><a href="#">Next</a></li>
								</ul>
							</div>
						</div>
					</div><!--/blog-post-area-->

					<div class="rating-area">
						<ul class="ratings">
							<li class="rate-this">Rate this item:</li>
							<li>
								<div class="rate">
									<div class="vote">
										<div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
										<div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
										<div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
										<div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
										<div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
										<span class="rate-np">{{empty($avgRate) ? 0 : $avgRate}}</span>
									</div> 
								</div>
							</li>
							

							<li class="color">({{count($data->Rates)}} votes)</li>
						</ul>
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="images/blog/socials.png" alt=""></a>
					</div><!--/socials-share-->

					<div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" src="images/blog/man-one.jpg" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Annie Davis</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div>
					<!--Comments-->

					

					<div class="response-area">
						
						<h2>{{count($dataComments)}} RESPONSES</h2>
						
						<ul class="media-list">
						@if(count($dataComments) > 0)
							@foreach($dataComments as $comment)
								@if($comment->idComment == 0)
									<li class="media">
										<a class="pull-left" href="#">
											<img width="50px" class="media-object" src="{{asset('/upload/'.$comment->User->avatar)}}" alt="">
										</a>
										<div class="media-body">
											<ul class="sinlge-post-meta">
												<li><i class="fa fa-user"></i>{{$comment->User->name}}</li>
												<li><i class="fa fa-clock-o"></i>{{date('h:i A', strtotime($comment->created_at))}}</li>
												<li><i class="fa fa-calendar"></i>{{date('M d, Y', strtotime($comment->created_at))}}</li>
											</ul>
											<p>{{$comment->comment}}</p>
											<textarea idBlog="{{$data->id}}" idComment="{{$comment->id}}" rows="1" placeholder="Reply....."></textarea>
											<a class="btn btn-primary reply" href="javascript:void(0)"><i class="fa fa-reply"></i>Reply</a>
										</div>
									</li>
									@if ( $comment->Replies )
										@foreach($comment->Replies()->orderBy('created_at', 'DESC')->get() as $rep1)
											<li class="media second-media">
												<a class="pull-left" href="#">
													<img width="50px" class="media-object" src="{{asset('/upload/'.$comment->User->avatar)}}" alt="">
												</a>
												<div class="media-body">
													<ul class="sinlge-post-meta">
														<li><i class="fa fa-user"></i>{{$rep1->User->name}}</li>
														<li><i class="fa fa-clock-o"></i>{{date('h:i A', strtotime($rep1->created_at))}}</li>
														<li><i class="fa fa-calendar"></i>{{date('M d, Y', strtotime($rep1->created_at))}}</li>
													</ul>
													<p>{{$rep1->comment}}</p>
													<textarea idBlog="{{$data->id}}" idComment="{{$comment->id}}" rows="1" placeholder="Reply....."></textarea>
													<a class="btn btn-primary reply" class="reply" href="javascript:void(0)"><i class="fa fa-reply"></i>Reply</a>
												</div>
											</li>
										@endforeach
									@endif
									
								@endif
							@endforeach
						@endif	
						</ul>	
									
					</div>
					
					
					<!--/Response-area-->
					<div class="replay-box">
						<div class="row">
							<div class="col-sm-4">
								<h2>Leave a replay</h2>
								<form>
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<input name="name" type="text" placeholder="write your name...">
									<div class="blank-arrow">
										<label>Email Address</label>
									</div>
									<span>*</span>
									<input name="email" type="email" placeholder="your email address...">
									<div class="blank-arrow">
										<label>Web Site</label>
									</div>
									<input name="city" type="email" placeholder="current city...">
								</form>
							</div>
							<div class="col-sm-8">
								<div class="text-area">
									<div class="blank-arrow">
										<label>Your Name</label>
									</div>
									<span>*</span>
									<textarea name="comment" rows="11"></textarea>
									<a idBlog="{{$data->id}}" class="btn btn-primary" id="comment" href="javascript:void(0)">post comment</a>
								</div>
							</div>
						</div>
					</div><!--/Repaly Box-->
				</div>	
			</div>
		</div>
	</section>

	<script>
		$(document).ready(function(){

			$(document).on('click',"#comment",function(){
				doComment($("textarea[name='comment']").val(),$(this).attr("idBlog"),0),null;
			})

			$(document).on('click',".reply",function(){
				doComment($(this).prev().val(),$(this).prev().attr("idBlog"),$(this).prev().attr("idComment"),$(this).prev());
			})

			function doComment(message,idBlog,idComment,ele){
				if({{empty(Auth::user()) ? 1 : 0}}){
					window.location.href="/member/login";
				}

				let data = {
					'comment' : message,
					'idBlog' : idBlog,
					'idComment' : idComment
				};
				$.ajax({
                            url: '/member/comment',
                            data: {
                                 'data' : JSON.stringify(data),
                                 _token: '{{csrf_token()}}'
                            },
                            success: function(data) {
                                let res = JSON.parse(data);
								
								console.log(res.comment);
								if(res.comment){
									let content = 
										'<a class="pull-left" href="#">'
											+'<img width="50px" class="media-object" src="{{asset("/upload/".Auth::user()->avatar)}}" alt="">'
										+'</a>'
										+'<div class="media-body">'
											+'<ul class="sinlge-post-meta">'
												+'<li><i class="fa fa-user"></i>{{Auth::user()->name}}</li>'
												+'<li><i class="fa fa-clock-o"></i>'+res.comment.time+'</li>'
												+'<li><i class="fa fa-calendar"></i>'+res.comment.day+'</li>'
											+'</ul>'
											+'<p>'+res.comment.comment+'</p>'
											+'<textarea idBlog="'+res.comment.idBlog+'" idComment="'+res.comment.id+'" rows="1" placeholder="Reply....."></textarea>'
											+'<a class="btn btn-primary reply" href="javascript:void(0)"><i class="fa fa-reply"></i>Reply</a>'
										+'</div>'
										;
										console.log(content);

									if(res.comment.idComment == 0){
										
										$(document).find(".media-list").prepend(
											'<li class="media">'
											+content
											+'</li>'
										);
									}else{
										ele.parents("li.media").after(
											strData = '<li class="media second-media">'
											+content
											+'</li>'
										);	
										
									}
								}
					
								$(document).find(".response-area h2").html(res.count+" RESPONSES");
                            },
                            type: 'POST'
                         });
			}
		})

	</script>
	
	<script>
    	
    	$(document).ready(function(){
			//vote
			$('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().andSelf().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().andSelf().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );

			$('.ratings_stars').click(function(){

				if({{empty(Auth::user()) ? 1 : 0}}){
					window.location.href="/member/login";
				}else{
					var Values =  $(this).find("input").val();
					let idUser = "{{empty(Auth::user()) ? 0 : Auth::user()->id}}";
					let idBlog = "{{$data->id}}";
					
					let data = {
						'idUser' : idUser,
						'idBlog' : idBlog,
						'rate' : Values
					};
					$.ajax({
								url: '/member/rate',
								data: {
									'data' : JSON.stringify(data),
									_token: '{{csrf_token()}}'
								},
								success: function(data) {
									let res = JSON.parse(data);
									if(res.rateData.length > 0){
										alert(res.message);
										$(document).find("#rateInfor").html(res.rateData);
										$(document).find(".ratings").children("li.color").html(
											"({{count($data->Rates) + 1}} votes)"
										);
										$(document).find(".rate-np").html(res.avgRate);
									}
								
								},
								type: 'POST'
							});


					if ($(this).hasClass('ratings_over')) {
						$('.ratings_stars').removeClass('ratings_over');
						$(this).prevAll().andSelf().addClass('ratings_over');
					} else {
						$(this).prevAll().andSelf().addClass('ratings_over');
					}
				}

				
		    });
		});
    </script>
	@endsection