
				@extends('frontend.template.master')
				@section('content')
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>

						@if(count($data) > 0)
							@foreach($data as $row)
							<div class="single-blog-post">
								<h3>{{$row->title}}</h3>
								<div class="post-meta">
									<ul>
										<li><i class="fa fa-user"></i> {{$row->user->name}}</li>
										<li><i class="fa fa-clock-o"></i> {{date('h:i A', strtotime($row->created_at))}}</li>
										<li><i class="fa fa-calendar"></i>{{date('M d, Y', strtotime($row->created_at))}} </li>
									</ul>
									<?php
										$avgRate = $row->Rates->avg('rate');
									?>
									<span>
										<?php
											
												for($i=0; $i<5; $i++){
													echo '<i class="fa fa-star'.
														($avgRate==$i+.5?'-half':'').
														($avgRate<=$i?'-o':'').
														'" aria-hidden="true"></i>';
													echo "\n";
												}
										?>
									</span>
									
								</div>
								<a href="">
									<img src="{{asset('upload/'.$row->image)}}" alt="">
								</a>
								<?php
									echo html_entity_decode($row->description);
								?>
								
								<a class="btn btn-primary" href="{{route('detailBlog',[$row->id])}}">Read More</a>
							</div>
							@endforeach
						@endif
						
						
						<div class="pagination-area">
							<ul class="pagination">
								<li><a href="" class="active">1</a></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	@endsection