
	@include('frontend.template.header') 
	@include('frontend.template.left-control') 

				<div class="col-sm-9">
					<h3>User Update</h3>
					<div class="signup-form"><!--sign up form-->
					@if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
						<form action="#" method="POST" enctype="multipart/form-data">
							@csrf
							<input type="text"  name="name" value="{{$profile_data->name}}" class="form-control"  placeholder="Name">
							<input type="email" name="email" value="{{$profile_data->email}}" class="form-control"  placeholder="Email" readonly>
							<input type="password" name="password" class="form-control"  placeholder="password">
							<input type="password" name="repassword" class="form-control"  placeholder="confirm password">
							
				                <input type="text" name="phone" value="{{$profile_data->phone}}" class="form-control"  placeholder="phone">
				            
							
				                <input type="text" name="address" value="{{$profile_data->phone}}" class="form-control"  placeholder="address">
				           
							
				                <input type="file" name="avatar"  class="form-control">
				            
							<div>
				                <label >Select Country</label>
                                        <div>
                                            <select name="idCountry" >
                                            @foreach($country_data as $country)
                                                    <option value="{{$country->id}}" {{$country->id == $profile_data->idCountry  ? 'selected' : ''}}>{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
				            </div>                
							<button type="submit" class="btn btn-default">Update</button>
						</form>
					</div><!--/sign up form-->
				</div>	
			</div>
		</div>
	</section>
	
	
	

  
   
	@include('frontend.template.footer')