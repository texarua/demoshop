
    @include('frontend.template.header')
    
		<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						@if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
						<form action="/member/login" method="POST">
                            @csrf
                        	<input name="email" type="text" placeholder="Email" />
							@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							<input name="password" type="password" placeholder="Password" />
							@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						@if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
						<form action="/member/signup" method="POST" enctype="multipart/form-data">
                            @csrf
							<input type="text" name="name" value="{{old('name')}}" placeholder="Name"/>
							<input type="email" value="{{old('email')}}" name="email" placeholder="Email Address"/>
							<input type="password" name="password" placeholder="Password"/>
							<!-- @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror -->
                            <input type="password" name="repassword" placeholder="RePassword"/>
                            <input type="text" value="{{old('phone')}}" name="phone" placeholder="Phone"/>
                            <input type="text" value="{{old('address')}}" name="address" placeholder="Address"/>
                            <input type="file" name="avatar" />
                            <div>
                                        <label >Select Country</label>
                                        <div>
                                            <select name="idCountry" >
											@foreach($country_data as $country)
                                                    <option value="{{$country->id}}" >{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	@include('frontend.template.footer')
    
	