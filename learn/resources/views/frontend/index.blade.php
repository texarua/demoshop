
                @extends('frontend.template.master')
                @section('content')
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<div class="dataSearch">
						@if(count($listProduct))
							@foreach($listProduct as $pro)
							
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('upload/product/'.$pro->user->id.'/'.json_decode($pro->image)[0])}}" alt="" />
												
												<h2 style="{{($pro->status) ? 'text-decoration: line-through' : ''}}">{{$pro->price}}</h2>
												<p>{{$pro->name}}</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
												<h2>{{($pro->status) ? $pro->sale : $pro->price}}</h2>
													<p>{{$pro->name}}</p>
													<a product="{{$pro}}" href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
											<img src="{{asset('public/images/home/'.(($pro->status) ? 'sale.png' : 'new.png'))}}" class="new" alt="" />
										</div>
										<div class="choose">
											<ul class="nav nav-pills nav-justified">
												<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
												<li><a href="{{route('detail',[$pro->id])}}"><i class="fa fa-plus-square"></i>View</a></li>
											</ul>
										</div>
									</div>
								</div>
							@endforeach
						@endif
						</div>
					</div><!--features_items-->
					
					
					
					
					
				</div>
			</div>
		</div>
	</section>
	
	<script>
		$(document).ready(function(){
			$(".add-to-cart").on("click",function(){
				let product = JSON.parse($(this).attr("product"));
				product['count'] = 1;
				
				$.ajax({
					url: '{{route("order")}}',
					type: 'POST',

					data: {
						data : JSON.stringify({
							action : 'add',
							obj : product,
							
						}),
						_token : '{{csrf_token()}}'
				
					},
					error: function() {
						alert("Add to Cart Failed !");
					},
					
					success: function(data) {
						
						$(".countCart").html(data);
					}
					
				});
				


				
			})

			$(document).on("slideStop",".span2",function(){
				doSearch();
			})


			$(document).on("keyup","input.search",function(){
				doSearch();
			})

			$(document).on("change","select[name='status']",function(){
				doSearch();
			})

			function doSearch(){
				let arr = $(".search").toArray();
				let rangePrice = $(document).find(".span2").data('slider').getValue();
				let status = $(document).find("select[name='status']").val();
				let minPrice = rangePrice[0];
				let maxPrice = rangePrice[1];
				let dataSearch = {
					'name' : arr[0].value,
					'category' : arr[1].value,
					'brand' : arr[2].value,
					'minPrice' : minPrice,
					'maxPrice' : maxPrice, 
					'status' : status
				};
				$.ajax({
						url: '{{route("fsearch")}}',
						type: 'POST',
						data: {
							'data' : JSON.stringify(
                                dataSearch
                            ),
                            _token : '{{csrf_token()}}'
						},
						error: function() {
							alert("Change Cart Failed !");
						},
						
						success: function(data) {
							let res = JSON.parse(data);
							let content = '';
							if(res.length > 0){

								res.map(function(value){
									
									content += 
									'<div class="col-sm-4">'
										+'<div class="product-image-wrapper">'
											+'<div class="single-products">'
												+'<div class="productinfo text-center">'
													+'<img src="{{asset("upload/product/")}}/'+value.user.id+'/'+JSON.parse(value.image)[0]+'" alt="" />'
													
													+'<h2 style="'+ ((value.status) ? "text-decoration: line-through" : "") +'">'+value.price+'</h2>'
													+'<p>'+value.name+'</p>'
													+'<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>'
												+'</div>'
												+'<div class="product-overlay">'
													+'<div class="overlay-content">'
													+'<h2>'+((value.status) ? value.sale : value.price)+'</h2>'
														+'<p>'+value.name+'</p>'
														+'<a product="'+value+'" href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>'
													+'</div>'
												+'</div>'
												+'<img src="{{asset("public/images/home/")}}/'+((value.status) ? "sale.png" : "new.png")+'" class="new" alt="" />'
											+'</div>'
											+'<div class="choose">'
												+'<ul class="nav nav-pills nav-justified">'
													+'<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>'
													+'<li><a href="{{asset("")}}'+value.id+'"><i class="fa fa-plus-square"></i>View</a></li>'
												+'</ul>'
											+'</div>'
										+'</div>'
									+'</div>';
									
								})
								
							}
							$(document).find(".dataSearch").html(content);
						}
						
				});
			}

		})
	</script>
	
    @endsection