<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    protected $table = 'users';
    public $timestamp = true;
    protected $fillable = ['id', 'name', 'email','password','phone','address','idCountry','avatar','level'];
    
    public function Blogs(){
        return $this->hasMany('App\Models\Blog','idUser','id');
    }

    public function Comments(){
        return $this->hasMany('App\Models\Comment','idUser','id');
    }
}
