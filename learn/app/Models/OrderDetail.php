<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //
    protected $table = "orderdetails";
    protected $fillable = ['id','created_at','idUser','idProduct','count','price'];
    public $timestamp = true;
}
