<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    //
    protected $table='rates';
    protected $fillable = [
        'id','created_at','idBlog','idUser','rate'
    ];
    public $timestamp = true; 

    public function Blogs(){
        return $this->belongsto('App\Model\Blog','idBlog','id');
    }
}
