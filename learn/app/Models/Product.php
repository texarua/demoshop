<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";
    protected $fillable = ['id','name','created_at','price','idUser','idCategory','idBrand','status','sale','company','image','description'];
    public $timestamp = true;

    public function Categorys(){
        return $this->belongsto('App\Models\Category','idCategory','id');
    }

    public function Brands(){
        return $this->belongsto('App\Models\Brand','idBrand','id');
    }

    public function User(){
        return $this->belongsto('App\Models\User','idUser','id');
    }
}
