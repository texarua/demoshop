<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table="comments";
    public $timestamp = false;
    public $fillable= ['id','created_at','idBlog','idUser','comment','idComment'];
    public function Blogs(){
        return $this->belongsto('App\Models\Comment','idBlog','id');
    }

    public function User(){
        return $this->belongsto('App\Models\User','idUser','id');
    }

    public function Replies(){
        return $this->hasMany('App\Models\Comment','idComment','id');
    }
}
