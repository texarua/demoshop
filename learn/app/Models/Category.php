<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "categories";
    protected $fillable = ['id','created_at','name'];
    public $timestamp = true;

    public function Products(){
        return $this->hasMany('App\Models\Product','idCategory','id');
    }
}
