<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $table='blogs';
    protected $fillable = [
        'id','title','image','idUser','created_at','description','content'
    ];
    public $timestamp = true; 

    public function User(){
        return $this->belongsto('App\Models\User','idUser','id');
    }

    public function Comments(){
        return $this->hasMany('App\Models\Comment','idBlog','id');
    }


    public function Rates(){
        return $this->hasMany('App\Models\Rate','idBlog','id');
    }

    
}
