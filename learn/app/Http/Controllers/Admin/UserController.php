<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    protected $updateParams = [
        'name',
        'email',
        'password',
        'phone',
        'address',
        'idCountry',
        'avatar'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //
        
        $country_data = Country::All();
        $profile_data= User::find($id);
        return view('admin/profile',compact('profile_data','country_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(UpdateUserRequest $request, int $id)
    {
        //
        // echo $request->user()->password;
        $updateResult = User::where('id',$id)->firstOrFail();

        
        $params = !empty($request->only($this->updateParams)) ? $request->all() : $request->only($this->updateParams);
        
        $params['password'] = strlen($request->password) > 0 ? Hash::make($request->password) : $request->user()->password;
        
        if ($request->hasFile('avatar')) {
            $file = $request->avatar;

            //Lấy Tên files
            $filename = $file->getClientOriginalName();
            
            $file->move('./upload', $file->getClientOriginalName());
            $params['avatar'] = $filename;
        }


        $updateResult->update($params);
        $updateResult->save();
        return redirect()->route('profile', [$id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
