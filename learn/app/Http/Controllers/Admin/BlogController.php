<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\AddBlogRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;


class BlogController extends Controller
{   
    protected $blogParams = [
        'id',
        'title',
        'image',
        'idUser',
        'created_at',
        'description',
        'content',
    ];
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(?Request $request)
    {
        //
        $blog_data = User::find(Auth::user()->id)->Blogs()->paginate(3);

        //Phan if de xu ly search ajax 
        if(!empty($request->data)){
            $params = json_decode($request->data,false);
            $blog_data = User::find(Auth::user()->id)->Blogs()->where('title','like','%'.$params->key.'%')->paginate(3);
            echo json_encode($blog_data);
        }else{
            //Lay du lieu ban dau
            return view('admin/blog/list',compact('blog_data'));
        }
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin/blog/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddBlogRequest $request)
    {
        //
        $params = !empty($request->only($this->blogParams)) ? $request->all() : $request->only($this->blogParams);
        $params['idUser'] = $request->user()->id;
        $params['description'] = htmlentities($request->description);
        $params['content'] = htmlentities($request->content);
        if ($request->hasFile('image')) {
            $file = $request->image;

            //Lấy Tên files
            $filename = $file->getClientOriginalName();
            
            $file->move('./upload', $file->getClientOriginalName());
            $params['image'] = $filename;
        }
        Blog::create($params);
        return redirect()->route('blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        //  
        $data = Blog::find($id);
        if(empty($data)){
            return redirect()->route('blog');
        }
        return view('admin/blog/edit',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(AddBlogRequest $request)
    {
        //
        $blog = Blog::find($request->id);
        $params = !empty($request->only($this->blogParams)) ? $request->all() : $request->only($this->blogParams);
        $params['description'] = htmlentities($request->description);
        $params['content'] = htmlentities($request->content);
        if ($request->hasFile('image')) {
            $file = $request->image;

            //Lấy Tên files
            $filename = $file->getClientOriginalName();
            
            $file->move('./upload', $file->getClientOriginalName());
            $params['image'] = $filename;
        }
        $blog->update($params);
        return redirect()->route('blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
        Blog::find($id)->delete();
        return redirect()->route('blog');
    }
}
