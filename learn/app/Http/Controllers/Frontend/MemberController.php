<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\LoginRequest;
use App\Models\Country;
use App\Models\User;
use App\Models\Product;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\Facades\Image as Image;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'E:\xampp\composer\vendor\autoload.php';

class MemberController extends Controller
{   
 
    protected $updateParams = [
        'name',
        'email',
        'password',
        'phone',
        'address',
        'idCountry',
        'avatar'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $listProduct = Product::with('User')->get();
        return view('frontend/index',compact('listProduct'));
    }

    public function detail($id)
    {
        //
        $pro = Product::where('id',$id)->with('User')->firstOrFail();
        $pro->image = json_decode($pro->image);
        if(empty($pro)){
            return redirect()->route('index');
        }
       
        return view('frontend/product/product-detail',compact('pro'));
    }

    public function cart(){
        return view('frontend/product/cart');
    }

    public function order(Request $request){
        $params = json_decode($request['data']);
        $proOrder = null;
        $data = null;
        if(!session()->exists('cart')){
            $data = [
                'listOrder' => [],
                'total' => 0
            ];
            session(['cart' => $data]);
        }else{
            $data = session('cart');
        }
        if($params->action == 'add' ){
            $proOrder = $params->obj;
            //Set Lai gia tri cho price New hoac sale
            $proOrder->price = ($proOrder->status) ? $proOrder->sale : $proOrder->price;
            $this->addToCart($proOrder);
        }
        if($params->action == 'change' ){
            $this->changeQuality($params->id, $params->math);
        }
    }
    
    function changeQuality($id,$math){
        $data = session('cart');
        $totalEdit = 0; 
        if($math == "+"){
            $data['listOrder'][$id]->count++;
            $data['total'] += $data['listOrder'][$id]->price;
        }
        else if($math == "-"){
            $data['listOrder'][$id]->count--;
            $data['total'] -= $data['listOrder'][$id]->price;
            if(!$data['listOrder'][$id]->count){
                unset($data['listOrder'][$id]);
            }
        }
        else if($math == "del"){
            $data['total'] -= $data['listOrder'][$id]->price * $data['listOrder'][$id]->count;
            unset($data['listOrder'][$id]);
        }
        
        else{
            $data['total'] -= $data['listOrder'][$id]->price * $data['listOrder'][$id]->count;
            if(is_numeric($math) && $math > 0){
                $data['listOrder'][$id]->count = $math;
                $data['total'] += $data['listOrder'][$id]->price * $math;
            }else{
                unset($data['listOrder'][$id]);
            }    
        }
        session(['cart' => $data]);   
        echo json_encode([
            'product' => isset($data['listOrder'][$id]) ? $data['listOrder'][$id] : 0,
            'totalBill' => $data['total']
        ]);
    }

    function addToCart($product){
        $flg = false;
        $total = 0;
        $data = session('cart');
        if(isset($data['listOrder'][$product->id])){
            $data['listOrder'][$product->id]->count ++;
            $flg = true;     
         }
        if(!$flg){
            $data['listOrder'][$product->id] = $product;
        }
        $data['total'] += $product->price;
        session(['cart' => $data]);
        echo sizeof($data['listOrder']);
    }

    public function payOneStep(Request $request){
        return $this->pay(Auth::user());  
     }
 
     public function pay($user){
         if(session()->exists('cart')){
             $listOrder = session('cart')['listOrder'];
             $paramsOrder = [];
             foreach(session('cart')['listOrder'] as $key => $value){
                 $paramsOrder[] = [
                     'idUser' => $user->id,
                     'idProduct' => $key,
                     'count' => $value->count,
                     'price' => $value->price
                 ];
             }
             OrderDetail::insert($paramsOrder);

             //Send Mail Detail Order
          $mail = new PHPMailer(TRUE);
          /* Open the try/catch block. */
          $mail->isSMTP();
          $mail->Host = 'smtp.gmail.com';
          $mail->SMTPSecure='SSL';
          $mail->Username='texarua2';
          $mail->Password='thuidiem2';
          $mail->SMTPAuth = true;
          /* Set the mail sender. */
          $mail->setFrom('texarua2@gmail.com', 'Admin');
  
         /* Add a recipient. */
          $mail->addAddress('123@gmail.com','123');
  
          /* Set the subject. */
          $mail->Subject =' Detail Order';
             
          /* Set the mail message body. */
          $mail->Body = $this->contentOrder($user,session('cart'));

          $mail->IsHTML(true); 

          /* Finally send the mail. */
          $mail->send();

        //Huy session sau khi thanh toan xong
             session()->forget('cart');
             return redirect()->route('checkout');
         }else{
             return redirect()->route('index');
         }
 
     }

     public function contentOrder($user,$listDetail){
        return view('frontend/templateMail',compact('user','listDetail'));
        }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AddUserRequest $request)
    {
        $flag = false;
        if(isset($request['submit'])){
            $flag = true;
        }
        $params = !empty($request->only($this->updateParams)) ? $request->all() : $request->only($this->updateParams);
        
        $params['password'] = strlen($request->password) > 0 ? Hash::make($request->password) : $request->user()->password;
        if ($request->hasFile('avatar')) {
            $file = $request->avatar;

            //Lấy Tên files
            $filename = $file->getClientOriginalName();
            
            $file->move('./upload', $file->getClientOriginalName());
            $params['avatar'] = $filename;
        }
        $params['level'] = 0;
        $user = User::create($params);
        
        if($flag){
            //xu ly thanh toan thanh cong
              return  $this->pay($user);
        }
        if (Auth::attempt(['email' => $user->email, 'password' => $user->password])) {
            // Authentication passed...
            return redirect()->intended('/blog');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

   


    public function checkout(){
        return view('frontend/product/checkout');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $country_data = Country::All();
        $profile_data= User::where('id',$id)->firstOrFail();
        if(empty($profile_data)){
            return redirect()->route('fblog');
        }
        return view('frontend/profile',compact('profile_data','country_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        //Co the viet function dung chung de update profile ben admin + member
        $updateResult = User::where('id',$id)->firstOrFail();

        
        $params = !empty($request->only($this->updateParams)) ? $request->all() : $request->only($this->updateParams);
        
        $params['password'] = strlen($request->password) > 0 ? Hash::make($request->password) : $request->user()->password;
        $params['avatar'] = $updateResult->avatar;
        if ($request->hasFile('avatar')) {
            $file = $request->avatar;

            //Lấy Tên files
            $filename = $file->getClientOriginalName();
            
            $file->move('./upload', $file->getClientOriginalName());
            $params['avatar'] = $filename;
        }


        $updateResult->update($params);
        $updateResult->save();
        return redirect()->route('fprofile', [$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
