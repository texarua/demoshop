<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;


use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\LoginRequest;
use App\Models\Country;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;



class LoginController extends Controller
{
    //

    public function doLogin(LoginRequest $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            if(empty(session('link'))){
                return redirect("/blog");
            }
            // return redirect()->intended('/blog');
           return redirect(session('link'));
        }else{
            return redirect("/flogin");
        }
    }

    public function logout(){
        Auth::logout();
        return redirect("/member/login");
    }
    

    public function showLogin()
    {
        //
        session(['link' => url()->previous()]);
        $country_data = Country::All();
        return view('frontend/login',compact('country_data'));
    }
}
