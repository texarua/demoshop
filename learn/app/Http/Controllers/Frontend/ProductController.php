<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddProductRequest;
use App\Http\Requests\UpdateProductRequest;

use Intervention\Image\Facades\Image as Image;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;

class ProductController extends Controller
{

    protected $productParams=[
        'name','price','idUser','idCategory','idBrand','status','sale','company','image','description'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $listProduct = Product::with('Categorys','Brands')->get();
        return view('frontend/product/list',compact('listProduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $listCategories = Category::All();
        $listBrands = Brand::All();
        
        return view('frontend/product/create',compact('listCategories','listBrands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddProductRequest $request)
    {
        //
        // print_r($request->all());
        if(empty($request->user())){
            return redirect()->route('flogin');
        }
        
        $params = !empty($request->only($this->productParams)) ? $request->all() : $request->only($this->productParams);

        $nameImage = $this->getNameImage($request);
        
        $params['image'] = json_encode($nameImage);
        $params['idUser'] = $request->user()->id;
        $product = Product::create($params);
        return redirect()->route('listProduct');
    }

    public function search(Request $request){
        $data = json_decode($request->data);
        $listSearch = Product::with('Categorys','Brands','User')->where('name','like','%'.$data->name.'%')
        
        ->whereHas(
            'Categorys', function($cate) use ($data){
               return $cate->where('name','like','%'.$data->category.'%');
           }
        )->whereHas(
                'Brands', function($brand) use ($data){
                return $brand->where('name','like','%'.$data->brand.'%');
            }
        )
        ->when($data->status == '1', function($pro) use ($data){
                return $pro->where('status', 1)->whereBetween('sale',[$data->minPrice,$data->maxPrice]);
        })
        ->when($data->status == '0', function($pro) use ($data){
            return $pro->where('status', 0)->whereBetween('price',[$data->minPrice,$data->maxPrice]);
        })
        
        ->get();

        echo json_encode($listSearch);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $listCategories = Category::All();
        $listBrands = Brand::All();
        $product = Product::where('id',$id)->with('Categorys','Brands')->firstOrFail();
        if(empty($product)){
            return redirect()->route('listProduct');
        }
        return view('frontend/product/edit',compact('product','listCategories','listBrands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $product = Product::Where('id',$id)->firstOrFail();
        $params = !empty($request->only($this->productParams)) ? $request->all() : $request->only($this->productParams);
        
        $nameImage = $this->getNameImage($request);

        $merge = [];
        if(isset($params['imgEdit']) && count($params['imgEdit'])){
            $merge = array_merge($nameImage, $params['imgEdit']);
        }
        if(!count($merge)){
            $params['image'] = json_decode($product->image);  
        }
        $params['image'] = json_encode($merge);
        $product->update($params);    
        return redirect()->route('listProduct');
    }

    public function getNameImage(Request $request){
        $idUser = $request->user()->id;
        $nameImage = [];
        
        if($request->hasfile('image'))
        {
            
            $timeImage = strtotime(date('Y-m-d H:i:s'));

            $pathDirectory = public_path('upload\product\\'.$idUser);
            if(!is_dir($pathDirectory)){
                
                mkdir($pathDirectory, 0755, true);
                
            }
            
            foreach($request->file('image') as $image)
            {

                $name = $timeImage."_".$image->getClientOriginalName();
                $name_2 = "small_".$timeImage."_".$image->getClientOriginalName();
                $name_3 = "large_".$timeImage."_".$image->getClientOriginalName();

                //$image->move('upload/product/', $name);
                
                $path =  $pathDirectory.'\\'.$name;
                $path2 = $pathDirectory.'\\'.$name_2;
                $path3 = $pathDirectory.'\\'.$name_3;
                
                
                
                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                
                $nameImage[] = $name;

            }
            
        }
        return $nameImage;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Product::where('id',$id)->firstOrFail()->delete();
        return redirect()->route('listProduct');
    }

   
}
