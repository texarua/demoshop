<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\User;
use App\Models\Rate;

class BlogController extends Controller
{
    //
    public function index(){
       $data = Blog::with('User','Rates')->orderBy('created_at','DESC')->get();
       return view('frontend/blog/blog',compact('data'));
    }

    public function show(int $id){
        $data = Blog::Where('id',$id)->with('User','Rates')->first();
        if(empty($data)){
            return redirect()->route('fblog');
        }
        $dataComments = Comment::where('idBlog',$id)->with('User')->orderBy('created_at', 'DESC')->get();
        return view('frontend/blog/detail-blog',compact('data','dataComments'));
    }

     public function comment(Request $request){
        $data = json_decode($request->data, true);
        $data['idUser'] = $request->user()->id;
        $comment = Comment::create($data);
        $comment->save();
        $comment->user = User::where('id',$data['idUser'])->get();  
        $comment->time = date("h:i A", strtotime($comment->created_at));
        $comment->day = date("M d, Y", strtotime($comment->created_at));
        $count = count(Comment::All());
        echo json_encode(['comment'=> $comment, 'count' => $count]);
        
    }

    public function rate(Request $request){
        $params = json_decode($request->data, true);
        $rate = null;
        $message = '';
        $rateData = '';
        $avgRate = 0;
        if(Rate::where('idUser', $params['idUser'])->where('idBlog',$params['idBlog'])->exists()){
            $message = 'Bạn đã đánh giá Blog này !';
        }else{
            $rate = Rate::create($params);
            if(!empty($rate)){
                $message = 'Đánh giá thành công !';
                $avgRate = round(Rate::where('idBlog',$params['idBlog'])->avg('rate'), 1);
                for($i=0; $i<5; $i++){

                    $rateData.= '<i class="fa fa-star'.
                        ($avgRate==$i+.5?'-half':'').
                        ($avgRate<=$i?'-o':'').
                        '" aria-hidden="true"></i>'."\n";
                }
            }
        }
        echo json_encode(['data' => $rate,'message' => $message,'rateData' => $rateData,'avgRate' => $avgRate]);
    }
}
