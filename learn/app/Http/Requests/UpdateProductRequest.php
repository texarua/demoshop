<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'price'=>'required|integer|min:0',
            'sale'=>'required|integer|min:0',
            'company'=>'required|min:6|max:20',
            'tmp' => 'integer|max:3',
            'image' => 'nullable',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
    public function messages()
    {
        
        return [
            'tmp.max' => 'image phải không quá :max file',
            'image.required' => 'chưa chọn ảnh',
            'image.max' => ':attribute không được quá :max file',
            'required'=>':attribute Không được để trống',
            'max'=>':attribute Không được quá :max',
            'integer' => ':attribute phải là số',
            'min' => ':attribute Không được nhỏ hơn :min',
            'image.*.required' => 'Chưa chọn ảnh',
            'image.*' => ':attribute phai la hình ảnh',
            'image.*.mimes' => ':attribute phai dinh dang như sau:jpeg,png,jpg,gif',
            'image.*.max' => ':attribute Maximum file size to upload :max',
        ];
    }
    protected function prepareForValidation() 
    {
        $countImageEdit = isset($this->imgEdit) ? count($this->imgEdit) : 0;
        $countImage = isset($this->image)  ? count($this->image) + count($this->imgEdit) : $countImageEdit;
        $this->merge(['tmp' => $countImage ]) ;
    } 
}
