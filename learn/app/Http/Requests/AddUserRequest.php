<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required|min:5|max:20',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|max:20',
            'repassword'=>'same:repassword',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
    public function messages()
    {
        return [
            'required'=>':attribute Không được để trống',
            'max'=>':attribute Không được quá :max ký tự',
            'min' => ':attribute Không được nhỏ hơn :min',
            'avatar.required' => 'Chưa chọn ảnh',
            'avatar' => ':attribute phai la hình ảnh',
            'mimes' => ':attribute phai dinh dang như sau:jpeg,png,jpg,gif',
            'avatar.max' => ':attribute Maximum file size to upload :max',
            
        ];
    }
}
